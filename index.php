<?php
namespace app;
set_time_limit(0);
include_once("models/Advert.php");
include_once("models/CacheLog.php");
use app\models\Advert;

Program::start();



class Program{
    private static $url_base = "https://www.avito.ru";
    private static $url_sell_msk = "https://www.avito.ru/moskva/kvartiry/prodam?view=list"; // in list view 100 per page
    private static $url_rent_msk = "https://www.avito.ru/moskva/kvartiry/sdam?view=list";

    /**
     * Точка входа в скрип
     */
    public static function start()
    {
        if (\CacheLog::isNew()) {
            //echo "ALL GOOD";
        } else {
            Advert::resetAll();
            $html_content_sell = self::getHtml(self::$url_sell_msk, self::$url_base);
            $objects = self::load_list($html_content_sell, 'продажа', 'квартиры');

            $html_content_rent = self::getHtml(self::$url_rent_msk, self::$url_base);
            $objects = array_merge($objects, self::load_list($html_content_sell, 'аренда', 'квартиры'));

            \CacheLog::addLog();
        }
        //            5 квартир исходя из условий: на продажу в г. Москва от собственника с этажом между 2 и 7, со стоимостью не более 10 млн руб, отсортированной по цене в порядке уменьшения.
        $datas = Advert::loadAll("address_city = 'Москва' and user_type_id =(select id from user_type where name = 'Продавец')
            AND floor between 2 AND 7 AND price <= 10000000 ORDER BY price DESC LIMIT 5");

        $all = [];
        foreach ($datas as $data)
            $all[] = $data->prepareToCian();

        if (isset($_GET['json']))
            self::returnFile(json_encode($all, JSON_UNESCAPED_UNICODE), 'file.json');
        else {
            $xml_data = new \SimpleXMLElement('<?xml version="1.0"?><feed><feed_version>2</feed_version></feed>');
            self::array_to_xml($all, $xml_data);
            self::returnFile($xml_data->asXML(), 'file.xml');
        }

    }
    /**
     * Обертка над file_get_contents, для обхода защиты AVITO
     * @param $url
     * @param $refer
     * @return bool|string
     */
    private static function getHtml($url,$refer){
        $opts = array(
            'http'=>array(
                'method'=>"GET",
                'header'=>"referer: $refer\r\n".
                    "Accept:text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8\r\n".
                    "Accept-Language:ru-RU,ru;q=0.8,en-US;q=0.6,en;q=0.4\r\n".
                    "Connection:keep-alive\r\n".
                    "Host:www.avito.ru\r\n".
                    "Upgrade-Insecure-Requests:1\r\n".
                    "User-Agent:Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36\r\n".
                    "Cookie:u=26ien0yp.1geo90c.fj4rl3ko1h; dfp_group=72; _ym_uid=1504297427255307950; __gads=ID=c21f4d29e8517b06:T=1504301019:S=ALNI_MaR16nmB7-THZ7J5ziC_WgtKC4zqg; f=5.0c4f4b6d233fb90636b4dd61b04726f147e1eada7172e06c47e1eada7172e06c47e1eada7172e06c47e1eada7172e06c47e1eada7172e06c47e1eada7172e06c47e1eada7172e06c47e1eada7172e06c47e1eada7172e06c47e1eada7172e06c0df103df0c26013a0df103df0c26013a2ebf3cb6fd35a0ac0df103df0c26013a0df103df0c26013a84dcacfe8ebe897bfa4d7ea84258c63d74c4a16376f87ccd897baa7410138eadfb0fb526bb39450a46b8ae4e81acb9fae2415097439d4047fb0fb526bb39450a46b8ae4e81acb9fa34d62295fceb188dd99271d186dc1cd03de19da9ed218fe2d50b96489ab264edd50b96489ab264edd50b96489ab264ed46b8ae4e81acb9fabfb238445a07e2998258743a6bcb50539ac45890a173cdf7544138105445a2013006c16a8b1ceec89b6989f3b8b12820d95476e520b6870dd96c01c1a544f13a60768b50dd5e12c3c9ec8d4d4b5cab2b019158140faccaa1465d5650ed2fd5c1685428d00dc691fa9e82118971f2ed6494d66450ac1e72927ddd6d6fbdcc54b43de19da9ed218fe23de19da9ed218fe2c96aef5a21a80290f9edeefb45b5a55f2e3395d4baf5981713de726369c13761; weborama-viewed=1; auth=1; anid=1004839553%3B72d8339d1e1319159d7e13b1559ad4bd%3B1; view=list; _nfh=446859d27247dd66635c5207bd0cdc06; sessid=e79ac752624819b950cc8779d966b247.1504642780; sx=eNqdk0GW4jAMRO%2BSdS9krBClb5NWiAgG1KAQAf367qPMPJhmlpOXne3vKlX5q2px09Xe6nxmBHInMQT0AtX7VzVX79WxG4uN8%2BowFxZiNFXyoqaCykTVW7Wp3lMN2GRYY%2Fv9Vn3wjKLTXbcuWITdYjsX5AeyOac83symCyzroKgOwoK87NYfyFWb2lUgu3SzerpfrZO4WwMnQFRKeSATrFO%2BULnOo6m5MjI7B5qApPCryhoDWT7Xu6YjbK5OzFrYwdD%2Bisy5%2B%2Bhk3OTaS5hVdTJSoYIYou2VSIvvnntupZ6mFTC4uWls5hDzQB7uN%2FG8Og6Tu2PMBcAEUdE9bP0jsl0HctO53mvOPLBSGEKBYuFHnunU1%2F2ot6Y%2BkS%2Fjk1gtQDFxICiv6TSpXlRe6n61K%2BNxiPFZjJ7MCMSfged9ztz7ZTt2kbWgOAOXGGjUQ%2BjVePM78Hzqj2lXk3YrLUh%2FsjSgZzoR1bTbTluZpMSYiSL3sKNG0QuTV2QDC3IoqR3y0DBHQUyidxaH9Gk88Z73hAccRNziK2RQQLnEZeYvyDYtyO5%2Bb9taT6KkMagl6wCa%2BH8gUyJamn7ef87H%2FHnqNHwVi3lGqI74TDyqMnQT5SaaIE60pB6240mIIvxAYiZc4knn%2B%2F3g0suFRSEOFYooI4YHcuqnvE%2Bn3W3LIUyRhARiPcgQ%2F18kATSr5fFkWad2mjOXNQNg6CPg6Pwz8Y%2BPud71fT%2FeYHmIIXVJOgqvIR%2FlJzKtKX1%2F%2FwKNTVDb; _ym_isad=1; nps_sleep=1; v=1504714318; crtg_rta=; _ym_visorc_34241905=b; _ga=GA1.2.100833329.1504297427; _gid=GA1.2.209908893.1504642789; _gat_UA-2546784-1=1"
            )
        );
        $context = stream_context_create($opts);
        return file_get_contents($url, false, $context);
    }
    /**
     * @param $html_content_sell
     * @param $sell_type
     * @param $appart_type
     * @return Advert[]
     */
    private static function load_list($html_content_sell,$sell_type,$appart_type){
        $objects = [];
        $regex_pattern_url = '/<a class="description-title-link" id="\d+" href="([^"]+)" title="[^"]+">/';
        preg_match_all($regex_pattern_url, $html_content_sell, $matches);

        foreach($matches[1] as $url){
//            echo "$url";
            $htmL_data = self::getHtml(self::$url_base.$url,self::$url_sell_msk);
            $object = new Advert([
                'appart_type'=>$appart_type,
                'sell_type'=>$sell_type,
                'url'=>$url
            ]);
            $object->loadViaHtml(
                preg_replace("/[\n\r]/","",$htmL_data)
            );
//            var_dump($object);
            if($object->save()) {  // UNCOMMENT FOR debug-output
//                 echo "$url SAVED \r\>";
                $objects[] = $object;

            }
            else {
//              echo "$url ERROR \r\n";
            }

        }
        return $objects;
    }

    /**
     * Преобразование в XML
     * @param $data
     * @param $xml_data
     */
    private static function array_to_xml( $data, &$xml_data ) {
        foreach( $data as $key => $value ) {
            if( is_numeric($key) ){
                $key = 'object'.$key; //dealing with <0/>..<n/> issues
            }
            if( is_array($value) ) {
                $subnode = $xml_data->addChild($key);
                self::array_to_xml($value, $subnode);
            } else {
                $xml_data->addChild("$key",htmlspecialchars("$value"));
            }
        }
    }
    private static function returnFile($txt,$name){
        header ('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename='.$name);
        echo $txt;
    }
}

?>