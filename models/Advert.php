<?php
namespace app\models;
include_once("PdoRepository.php");
include_once("DataClass.php");
include_once("GeoLight.php");

/**
 * Класс описывающий главную модель объявления
 * Class Advert
 * @package app\models
 */
class Advert {
    private static $tablename = "advert";
    public $id,$url;
    public $user_type,$appart_type,$sell_type;
    public $address_full,$address_city,$address_street,$address_house,$address_pos,$area;
    public $floor,$floor_max,$metro_name,$price,$description,$user_name,$room;
    /**
     * Advert constructor.
     */
    public function __construct()
    {
        $arguments = func_get_args();
        if(!empty($arguments)) {
//            if(isset($arguments[0]['url'])){
//                if($data = $this->loadData("url = '{$arguments[0]['url']}'"))
//                    $arguments[0] = $data+$arguments;
//            }
            foreach ($arguments[0] as $key => $property)
                if (property_exists($this, $key))
                    $this->{$key} = $property;

        }
    }

    /**
     * Загрузка всех подходящих моделей в память
     * @param string $where
     * @return Advert[]
     */
    public static function loadAll($where = '1=0'){
        $connection = PDORepository::getConnection();
        $sql = "SELECT 
                      t.id,area, address_city, address_street, address_house,
                     address_pos, sell_type_id, appart_type_id, room, 
                     floor, floor_max, metro_name, price, description,
                      user_name, user_type_id, address_full, url,ut.name as user_type,
                      st.name as sell_type, at.name as appart_type
                      
                       from ".self::$tablename." t
                       left join user_type ut on ut.id = t.user_type_id
                       left join sell_type st on st.id = t.sell_type_id
                       left join appart_type at on at.id = t.appart_type_id
                       where $where";
        $stmt = $connection->prepare($sql);
        $stmt->execute();
        //var_dump($stmt->errorInfo());
        $items =  $stmt->fetchAll();
        $objects = [];
        foreach ($items as $item){
            $objects[] = new self($item);
        }
        return $objects;

    }
    /**
     * @return bool
     */
    public function save(){
        $connection = PDORepository::getConnection();
        $tempGeo = new GeoLight($this->address_full);

        if($this->id){ // UPDATE
            $sql = "UPDATE advert SET
                area = :area,address_full = :address_full, address_city = :address_city,  address_street = :address_street,
                address_house = :address_house,address_pos = :address_pos,
                appart_type_id = :appart_type_id,room = :room,floor = :floor,floor_max = :floor_max,
                metro_name = :metro_name,price = :price,description = :description,user_name = :user_name,
                user_type_id = :user_type_id,sell_type_id = :sell_type_id,url = :url
            WHERE id = :id";
        } else { // INSERT
            $sql = "INSERT INTO advert (
                area, address_city, address_street, address_house,
                 address_pos, sell_type_id, appart_type_id, room, 
                 floor, floor_max, metro_name, price, description,
                  user_name, user_type_id, address_full, url
              ) VALUES (
                  :area, :address_city, :address_street, :address_house,
                  :address_pos,:sell_type_id, :appart_type_id, :room,
                  :floor,:floor_max, :metro_name, :price, :description,
                   :user_name,:user_type_id, :address_full, :url
               )";
        }
        $stmt = $connection->prepare($sql);
        if($this->id)
            $stmt->bindValue(':id', $this->id, \PDO::PARAM_STR);

        $stmt->bindValue(':area', $this->area, \PDO::PARAM_STR);
        $stmt->bindValue(':address_full', $this->address_full, \PDO::PARAM_STR);
        $stmt->bindValue(':address_city', $tempGeo->city, \PDO::PARAM_STR);
        $stmt->bindValue(':address_street', $tempGeo->street, \PDO::PARAM_STR);
        $stmt->bindValue(':address_house', $tempGeo->house, \PDO::PARAM_STR);
        $stmt->bindValue(':address_pos', $tempGeo->point, \PDO::PARAM_STR);
        $stmt->bindValue(':room', $this->room, \PDO::PARAM_INT);
        $stmt->bindValue(':floor', $this->floor, \PDO::PARAM_INT);
        $stmt->bindValue(':floor_max', $this->floor_max, \PDO::PARAM_INT);
        $stmt->bindValue(':metro_name', $this->metro_name, \PDO::PARAM_STR);
        $stmt->bindValue(':price', intval(str_replace(' ', '', $this->price)), \PDO::PARAM_INT);
        $stmt->bindValue(':description', preg_replace('/<[^>]*>/', '',$connection->quote($this->description)), \PDO::PARAM_STR);
        $stmt->bindValue(':user_name', $this->user_name, \PDO::PARAM_STR);
        $stmt->bindValue(':url', $this->url, \PDO::PARAM_STR);

        $stmt->bindValue(':user_type_id', UserType::initByName($this->user_type)->id, \PDO::PARAM_INT);
        $stmt->bindValue(':sell_type_id', SellType::initByName($this->sell_type)->id, \PDO::PARAM_INT);
        $stmt->bindValue(':appart_type_id', AppartType::initByName($this->appart_type)->id, \PDO::PARAM_INT);

        if($stmt->execute()) {
            if (!$this->id)
                $this->id = $connection->lastInsertId();
            return true;
        }
        //var_dump($stmt->errorInfo());
//        var_dump($this);
        return false;
    }

    /**
     * Удаляел все объявления из базы. Возвращает кол-во удаленных записей
     * @return int
     */
    public static function resetAll(){
        $connection = PDORepository::getConnection();
        $stmt = $connection->prepare("DELETE from advert");
        $stmt->execute();
        return $stmt->rowCount();
    }

    /**
     * Загружает данные в модель из существующей страницчки с информацией на основе правил в $REGEX_PATTERNS
     * @param $html
     */
    public function loadViaHtml($html){
        foreach (self::$REGEX_PATTERNS as $key=>$pattern) {
            preg_match($pattern, $html,$tmp);
            if(isset($tmp[1]))
                $this->{$key} = trim($tmp[1]);
        }
    }

    public static $REGEX_PATTERNS = [
        "price" => "/>([^>]+)&nbsp;<span class=\"price-value-prices-list-item-currency_sign\">/m",
        "room" => "/<span class=\"item-params-label\">Количество комнат: <\/span>([^<]+)</m",
        "user_type" => "/seller-info-col\"> +<div class=\"seller-info-label\">([^<]+)<\/div>/m",
        "address_full" => "/<div class=\"seller-info-label\">Адрес<\/div> <div class=\"seller-info-value\">([^<]+)</m",
        "area" => "/<span class=\"item-params-label\">Общая площадь: <\/span>([^<]+)&nbsp;м²/m",
        "floor"=> "/<span class=\"item-params-label\">Этаж: <\/span>([^<]+)</m",
        "floor_max" => "/class=\"item-params-label\">Этажей в доме: <\/span>([^<]+)</m",
        "metro_name" => "/<i class=\"i-metro i-metro-msk-1\" title=\"[^\"]+\"><\/i>([^\(]+)\(/m",
        "description" => "/itemprop=\"description\">(.*?)<\/div/m",
            //"/<p>([^<]+)</m",
        "user_name" => "/seller-info-name\"> +<a href=[^>]+>([^<]+)</m",
            //"/title=\"Нажмите, чтобы перейти в профиль\">([^<]+)</m",
    ];


    /**
     * @return array
     */
    public function prepareToCian(){
       return [
            'Category'=>'flatSale',
            'Description'=>$this->description,
            'Address'=>$this->address_full,
            'FlatRoomsCount'=>$this->room,
            'TotalArea'=>$this->area,
            'FloorNumber'=>$this->floor,
            'Building'=>[
                'FloorsCount'=>$this->floor_max
            ],
            'BargainTerms'=>[
                'Price'=>$this->price
            ]
        ];
    }
}
