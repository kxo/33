<?php
namespace app\models;
/**
 * Class DataClass
 * @package app\models
 */
class DataClass{
    public $id,$name;

    /**
     * @param $name
     * @return DataClass
     */
    public static function initByNameFull($name,$tablename){
        $connection = PDORepository::getConnection();
        $stmt = $connection->prepare("SELECT id,name from $tablename where name = '$name'");
        $stmt->execute();
        $object = $stmt->fetchObject(self::class);
        if($object) return $object;
        else{
            $object = new self([
                'name'=>$name
            ]);
            $stmt = $connection->prepare("INSERT INTO $tablename (name) VALUES ('$name')");
            if($stmt->execute()) {
                $object->id = $connection->lastInsertId();
            }
//            var_dump($stmt->errorInfo());
            return $object;
        }
    }

    public function __construct()
    {
        $arguments = func_get_args();
        if(!empty($arguments))
            foreach($arguments[0] as $key => $property)
                if(property_exists($this, $key))
                    $this->{$key} = $property;
    }
}
class AppartType extends DataClass{
    public static function initByName($name){
        $tablename = "appart_type";
        return self::initByNameFull($name,$tablename);
    }
}
class SellType extends DataClass{
    public static function initByName($name){
        $tablename = "sell_type";
        return self::initByNameFull($name,$tablename);
    }
}
class UserType extends DataClass{
    public static function initByName($name){
        $tablename = "user_type";
        return self::initByNameFull($name,$tablename);
    }
}