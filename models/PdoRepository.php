<?php
namespace app\models;
abstract class PDORepository
{
    const USERNAME = "admin";
    const PASSWORD = "153246";
    const HOST = "localhost";
    const DB = "33";

    /**
     * @return \PDO
     */
    public static function getConnection()
    {
        $username = self::USERNAME;
        $password = self::PASSWORD;
        $host = self::HOST;
        $db = self::DB;
        $connection = new \PDO("mysql:dbname=$db;host=$host", $username, $password,[\PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"]);
        return $connection;
    }
}
