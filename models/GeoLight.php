<?php /** Город, улица, дом, координаты */
namespace app\models;
/**
 * Class GeoLightObject
 * @package app\models
 */
class GeoLight {
    public $address_full;
    public $city,$street,$house, $point; // STRING IN 'Y X' format
    function __construct($address_full){
        $this->address_full= $address_full;
        $this->fill_data();
    }

    /**
     * Заполнение полей по $address_full
     * @return bool
     */
    private function fill_data(){
        $url ="https://geocode-maps.yandex.ru/1.x/?geocode=".$this->address_full;
        $xml_raw = file_get_contents($url);
        $resource = simplexml_load_string($xml_raw);
        $found = intval($resource->GeoObjectCollection->metaDataProperty->GeocoderResponseMetaData->found);
        if($found == 0) return false;
        $geo_object = $resource->GeoObjectCollection->featureMember->GeoObject;
        $address_object = $geo_object->metaDataProperty->GeocoderMetaData->Address;
        $this->point = (string)$geo_object->Point->pos; // #29500 PHPBUG
        foreach ($address_object->Component as $item) {
            switch ($item->kind) {
                case 'locality':
                    $this->city = (string)$item->name;
                    break;
                case 'street':
                    $this->street = (string)$item->name;
                    break;
                case 'house':
                    $this->house = (string)$item->name;
                    break;
            }
        }
        return true;
    }
}