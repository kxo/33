<?php

class CacheLog{
    /**
     * Возвращает факт наличия недавних данных
     * @return bool
     */
    public static function isNew(){
        $conn = \app\models\PDORepository::getConnection();
        $stmt = $conn->prepare("select exists( select * from cache_log where datetime >= DATE_SUB(NOW(), INTERVAL 10 MINUTE))");
        $stmt->execute();
        return boolval($stmt->fetchColumn());
    }

    /**
     * @return bool
     */
    public static function addLog(){
        $conn = \app\models\PDORepository::getConnection();
        $stmt = $conn->prepare("insert into cache_log () values()");
        return $stmt->execute();
    }

}